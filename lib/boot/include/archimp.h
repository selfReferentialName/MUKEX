#ifndef MBOOT_ARCHIMP_H
#define MBOOT_ARCHIMP_H

#include <stddef.h>

/* PAGESIZE is set in CFLAGS */

/* get a single page
 */
void* getpage (void);

/* string together pages
 * put them in virtual memory where it doesn't conflict with physical mem
 * ... is the list of pointers to pages
 */
void* stringpage (int, ...);

/* give a page back to the heap
 */
void freepage (void*);

/* logging functions
 */
void log_init (void);
void log (char*);
void log_warn (char*);
void log_error (char*);

/* wait for a certain number of
 * nanoseconds (ns) or
 * miliseconds (ms)
 */
void waitns (unsigned int);
void waitms (unsigned int);

#endif
