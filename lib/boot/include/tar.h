#ifndef MBOOT_H_RAMDISK
#define MBOOT_H_RAMDISK

typedef char ramdisk_t;	/* a ramdisk uses characters in the header */

/* this is guarenteed to have a char* field data
 * which points to the data in the file
 * and an interger type length
 */
typedef struct {
	char* d;
	long n;
} file_t;

/* initialize a ramdisk
 * for tar, this is a no-op (unless we cache headder positions)
 */
inline ramdisk_t* ramdisk_init (void* input)
{
	return (ramdisk_t*) input;
}

/* get the address of data in a file
 * in other radisk methods, this may require copying
 */
struct file ramdisk_index (ramdisk_t*, char*);

/* copy a named file to a specific pre-allocated address
 */
int ramdisk_copy (ramdisk_t*, char*, void*);

#endif
