/* tarfs.c
 * tar ramdisk operations
 * TODO: error checking
 * e.g. nonexistant files
 */

#include "tar.h"
#include "archimp.h"
#include "string.h"

/* convert an octal string into a long number
 * long should be over 36 bits long if the address space is significantly
 * larger than 36 bits
 * assumes strings are in ascii or a superset thereof
 * (also required for posix, so not a big deal)
 * s: the string to convert to octol
 * n: its length
 */
static long octol (char* s, size_t n)
{
	long o;
	o = 0;
	for (size_t i = 0; i < n; i++) {
		o <<= 3; /* string nubers are big endean */
		o += input[i] - '0';
	}
	return o;
}

/* get the address in the ramdisk
 * recursive
 * disk: the ramdisk to find said file in
 * s: the name of the file
 * o: the information about the file
 */
file_t ramdisk_index (ramdisk_t* disk, char* s)
{
	/* if the name is the same as the filename (starting at the first byte
	 * of the headder, we use this entry
	 * else we try the next one recursively
	 */
	if (strncmp(s, (char*) disk, 100)) {
		file_t o;
		o.d = (char*) disk + 512;
		o.n = octol((char*) disk + 124, 12);
		return o;
	} else {
		return ramdisk_index(disk + octol((char*) disk + 124, 12), s);
	}
}

/* copy a named file to a pre-allocated address
 * disk: the ramdisk to copy it from
 * s: the name of the file
 * ptr: the address to copy it to
 */
int ramdisk_copy (ramdisk_t* disk, char* s, void* ptr)
{
	/* first, find the file */
	file_t f;
	f = ramdisk_index(disk, s);
	/* then, memcpy */
	return memcpy(f.d, ptr, f.n);
}
