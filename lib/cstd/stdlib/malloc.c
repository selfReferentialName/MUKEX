/* malloc.c
 * the reference implementation of the rainalloc algorithm
 * this file is buggy and therefore not styalistically correct
 * TODO: get a replacement
 */

#include <stdint.h>
#include <stddef.h>
#include "arch/pages.h"

/* small size blocks */
static void*	bs[8];

/* large block heap, initialized by the architecture
 * what the initial form is is based on the OS kernel
 * for ndown, index 1 is the bitmap of fully free
 * index 0 is the partly free bitmap */
struct hnode	{
	struct {
		struct hnode*		down;
		uintptr_t			pagecount;
		uint64_t			sixdown[2];
		uint32_t			fivedown[2];
		uint16_t			fourdown[2];
		uint8_t				threedown[2];
		uint8_t				twodown;
	} left, right;
};
extern struct hnode __cstd_heaproot;
#define hroot __cstd_heaproot

/* allocate a large heap block
 * i.e. larger than a page
 * first, a recursive function for pwer of two sizes
 * size is the log base 2 of the size - the page size, rounded up,
 * nd is the heap node
 * hght is number of nodes between a page sized entry and node */
static void* malloc_lgr (uint8_t size, struct hnode* nd, uint8_t hght) {
	/* first, check to see if size is matched by nd's childs' size */
	if (size_lb == height)
		/* if it is, equal, we just need a valid child to return */
		return (nd->left.pagecount) ?
			(nd->left.down) :
			(nd->right.down);
	/* next, get how far off from aligned we are, on a scale of 1 to 6 */
	uint8_t misalign;
	misaign = (size - hght) % 6;
	/* also, get whether we need the full or partial bitmap */
	uint8_t is_penultamate;
	is_penultamate = (hght + misalign) == size;
	/* then, use this info to get the right bitmap */
	uint64_t bitmap[2];
	switch (misalign) {
		case 0:	bitmap[0] = nd->left.sixdown[is_penultamate];
				bitmap[1] = nd->right.sixdown[is_penultamate];
				break;
		case 1:	bitmap[0] = nd->left.fivedown[is_penultamate];
				bitmap[1] = nd->right.fivedown[is_penultamate];
				break;
		case 2:	bitmap[0] = nd->left.fourdown[is_penultamate];
				bitmap[1] = nd->right.fourdown[is_penultamate];
				break;
		case 3:	bitmap[0] = nd->left.threedown[is_penultamate];
				bitmap[1] = nd->right.threedown[is_penultamate];
				break;
		case 4:	if (is_penultamate) {
					bitmap[0] = (nd->left.twodown & 0xf0) >> 4;
					bitmap[1] = (nd->right.twodown & 0xf0) >> 4;
				} else {
					bitmap[0] = (nd->left.twodown & 0x0f) >> 0;
					bitmap[1] = (nd->right.twodown & 0x0f) >> 0;
				}
				break;
		case 5: if (is_penultamate) {
					bitmap[0] = (nd->left.pagecount
		/* default should never happen */
	}
static void* malloc_large (size_t size) {
	/* first, calculate 
