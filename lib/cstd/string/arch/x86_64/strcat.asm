	;; strcat.asm
	;; string concatenation
	;; strcat and strncat

section .text
global __strcat
global __strncat
extern __strncpy
extern __strlen

__strcat:
strcat:	push rax
	push rdx
	call strlen
	pop rsi
	mov rdi, [rsp]
	add rdi, rax		; get the registers into place and get rdi
				; to the place to append characters to
.loop:	cmp byte [rdi], 0
	movsb
	jne .loop		; copy characters until one is NULL
	pop rax
	ret

__strncat:
strncat:push rax
	push rdx
	push rdi
	call strlen
	add rax, [rsp + 16]	; rax is now where to copy
	pop rdi
	pop rdx
	call __strncpy
	pop rax
	ret
