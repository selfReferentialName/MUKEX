	;; strchr.asm
	;; implement strchr, strnchr, memchr, and strrchr

section .text
global __strchr
global __strnchr
global __memchr
global __strrchr
extern __strlen

__strnchr:
strnchr:push rax
	push rdx
	push rdi
	mov rax, rdx
	call __strlen
	pop rdi
	cmp rax, rdi
	cmovlt rdi, rax
	pop rdx
	pop rax
	jmp memchr

__strchr:
strchr:	push rax
	push rdx
	mov rax, rdx
	call __strlen
	mov rdi, rax
	pop rdx
	pop rax
;	jmp memchr

__memchr:
memchr:	mov rcx, rdi
	mov rdi, rdx
	repne scasb
	mov eax, 0
	cmove rax, rdi
	ret

__strrchr:
strrchr:push rax
	push rdx
	sub rsp, 8
	xor rax, rax
.loop:	mov [rsp], rax
	mov rax, [rsp + 16]
	call strchr
	test rax, rax
	jnz .loop
	pop rax
	add rsp, 16
	ret
