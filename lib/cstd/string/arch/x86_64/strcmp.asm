	;; strcmp.asm
	;; string comparison
	;; strcmp, strncmp, and memcmp
	;;
	;; for string comparison, there are two cases of note to x86
	;; in the most simple case, the strings are inequal
	;; in this case, repne cmpsb will return with ZF clear
	;; the more complex case is thet the strings are equal
	;; if this is the case, we need to check to see if there are two 0s
	;; in the same offset from the string
	;; because of this, we need the string length

section .text
global __strcmp
global __strncmp
global __memcmp
extern __strlen

__strcmp:
strcmp:	push rax
	push rdx			; save regs thet strlen may trash
	call __strlen
	mov rcx, rax			; get the maximum count to compare
	pop rsi
	pop rdi				; get the operands into string regs
scmp:	repne cmpsb			; compare
	mov rdx, -1
	xor rax, rax
	cmovgt rax, rdx
	salc
	ret

__strncmp:
strncmp:; first, get the minimmum test length
	push rax
	push rdx
	push rdi
	call __strlen
	mov rdi, [rsp]
	cmp rdi, rax
	cmovgt rdi, rax
	mov [rsp], rdi
	mov rax, [rsp + 8]
	call __strlen
	pop rdi
	cmp rdi, rax
	cmovgt rdi, rax
	pop rdx
	pop rax
;	jmp memcmp

__memcmp
memcmp:	mov rcx, rdi
	mov rdi, rax
	mov rsi, rdx			; operands into string regs
	jmp scmp			; share the body of ofther compares
