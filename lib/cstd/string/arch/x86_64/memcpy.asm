	;; memcpy.asm
	;; implements void* memcpy (void*, void*, size_t)

section .text
global __memcpy
global __memmove

__memmove:
__memcpy:
memcpy:	mov rcx, rdi
	mov rdi, rax
	mov rsi, rdx
	rep movsb			; the only benefit of CISC
	ret
