	;; strlen.asm
	;; string length
	;; implement the strlen function

section .text
global __strlen

__strlen:
strlen:	xor rcx, rcx
	dec rcx			; set rcx to the greatest possible length
	mov rdi, rax
	xor al, al
	repe scasb		; at the end of this, rdi points to 0 and
				; rcx is -1 - the string length
	inc rcx
	neg rcx			; rcx is now the string length
	mov rax, rcx
	ret
