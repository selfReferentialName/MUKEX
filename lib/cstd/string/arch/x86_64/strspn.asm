	;; strspn.asm
	;; implements strspn and strcspn

section .text
global __strspn
global __strcspn
global __strstr

__strspn:
strspn:	push rax
	push rdx
	call strlen
	mov rcx, rax		; compare only characters before terminator
	pop rsi
	pop rdi
	mov rax, rdi		; keep a copy of the address to get length
	repe cmpsb
	sub rdi, rax
	mov rax, rdi
	ret

__strcspn:
strcspnpush rax
	push rdx
	call strlen
	mov rcx, rax		; compare only characters before terminator
	pop rsi
	pop rdi
	mov rax, rdi		; keep a copy of the address to get length
	repne cmpsb
	sub rdi, rax
	mov rax, rdi
	ret
