	;; memset.asm
	;; implements memset

section .text
global __memset

__memset:
memset:	push rax
	mov rcx, rdi
	mov rdi, rax
	mov al, dl
	rep stosb
	pop rax
	ret
