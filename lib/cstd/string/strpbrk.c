/* strpbrk.c
 * implements strpbrk
 */

#include "__string.h"

/* find first instance of a character in chars in s
 * s: string to find a character in
 * chars: array of characters to find (0-terminated)
 */
char* __strpbrk (char* s, char* chars)
{
	char* first; /* the first instance so far */
	first = 0;
	for (int i = 0; chars[i] != 0; i++) {
		char* candidate; /* instance or NULL */
		candidate = __strchr(s, chars[i]);
		if (candidate) { /* if non-null (i.e. chars[i] is in
				    s) */
			if (first) { /* if not the first found so far */
				first = (candidate < first) ?
					canditate :
					first;
			} else { /* first character found */
				first = candidate;
			}
		}
	}
	return first;
}
