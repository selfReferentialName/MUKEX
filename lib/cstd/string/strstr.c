/* strstr.c
 * implements strstr
 */

#include "__string.h"

/* find an instance of sub_str in super_str
 * super_str: the string is searched
 * sub_str: the string to search
 */
char* __strstr (char* super_str, char* sub_str)
{
	size_t i;
	i = 0;
	size_t j;
	char* guess;
	guess = 0;
	while (super_str[i]) {
		if (guess) {
			if (sub_str[j] == super_str[i]) {
				/* if the sub_str is continued */
				j++;
			} else if (sub_str[j] == 0) {
				/* if the sub_str is over */
				return guess;
			} else {
				/* if the sub_str is broken */
				guess = 0;
			}
		} else {
			if (super_str[i] == sub_str[0]) {
				/* if the sub_str is starting */
				guess = sub_str + i;
				j = 1;
			} else {
				/* if the current super_str character is unimportant */
			}
		}
	}
	return 0; /* if we didn't find it and return earlier */
}
