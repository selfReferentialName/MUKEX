/* bdev.h
 * block device headder
 */

#ifndef __BDEV_H
#define __BDEV_H

#include <arch/pid.h>
#include <stddef.h>
#include <stdint.h>

/* block device descriptor
 */
struct bdev {
	void* buff;   /* a pointer to the shared memory */
	size_t bsize; /* the size of a block */
	pid_t dev;    /* process controling the device */
};

/* get a new block from the device
 * position must be kept track of by the process
 */
int block_new (const struct bdev device, uintmax_t block);

#endif
