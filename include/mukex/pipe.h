/* pipe.h
 * pipe operations
 * this headder file is intended to also be readable as documentation
 */

#include <arch/pid.h>

/* the raw structure of a pipe
 * it exists in shared memory
 */
struct pipe {
	short rd_end;   /* index read */
	short wr_end;   /* index written */
	pid_t rd_pid;   /* reading process */
	pid_t wr_pid;   /* writing process */
	char  mode;     /* bit 0: read, bit 1: write */
	char  buffer[]; /* raw character buffer */
};

/* c standard library file
 * only used as a pointer
 */
#define FILE pipe

/* read from a pipe
 */
int pread (void* dest, short bytes, const pipe* file);

/* write to a pipe
 */
int pwrite (const void* src, short bytes, pipe* file);

/* switch a pipe from read mode to write mode
 */
int pswitch (pipe* file);
