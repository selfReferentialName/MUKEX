#ifndef __KCALL_H
#define __KCALL_H

#include <arch/pagesize.h>
#include <arch/pid.h>
#include <bool.h>

void* getpage  (void);
void  freepage (void);
pid_t getpid   (void);
pid_t getppid  (void);
bool  ischild  (pid_t);
bool  kill9    (pid_t);
pid_t clone    (void);
pid_t fork     (void);
void  yield    (void);
bool  adopt    (pid_t, pid_t);
void  cswitch  (pid_t);
void  schedule (unsigned int, pid_t);
void  setsched (unsigned long int, target);

#endif
