Syscall List
============

Options
-------

 * yield
 ** switch to the scheduler
 ** possibly a special case of switch
 * switch
 ** switch to an arbitrary precess
 ** needed for an out-of-process scheduler
 * rpcall or rpc
 ** switch to a process without trashing/saving registers
 ** rpc would be a unified rpcall/rpret
 * rpret
 ** special case of rpcall done to the calling process
 * sched
 ** schedule a process for a set amount of time
 ** after that time, run a yield
 ** nesting, i.e. schedule another process in the time the one is scheduled
 *** enables libOS-style thread scheduling
 *** would require a yield syscall
 * adopt
 ** change parentship
 ** parentship gives permission for some syscalls
 * addppid
 ** add a new parent
 ** multiple parents is more powerfull
 ** multiple parents is hard to do correctly
 ** could have a set finite number of parents
 *** this would make syscall times vary
 **** bad for realtime, OK in other cases
 * exit
 ** kill a process
 * clone
 ** fork a process, don't use COW (i.e. share all pages)
 * fork
 ** fork a process, COW all unmarked pages
 * getpg
 ** get a page
 ** could have a userspace process controll paging
 *** would reduce speed but simplify swap
 *** hard to implement remove page on exit
 **** i.e. means one process has to be given orphan pages
 * sharepg
 ** offer a page to another process
 ** if accepted, the page becomes shared memory
 * givepg
 ** offer a page to another process
 ** if accepted, the page is transfered and taken from the giver
 * acceptpg
 ** accept a page offer
 *** i.e. by sharepg or givepg
 * freepg
 ** give a page back to the system
 ** could be a special case of givepg
 * wrio
 ** write to an io address
 * rdio
 ** read from an io address
 * lendpg
 ** offer a page, but reserve the right to mark it as not-present
 ** enables caching
 * cachepg
 ** load a physical page into the virtual address space
 * migrate
 ** change the domain of a process
 ** only can be run by a monarch or a supermonarch
 ** monarch can migrate process in domain to subdomain
 *** migrateUp
 ** supermonarch can migrate process from subdomain to domain
 *** migrateDown
 * tcreate
 ** form 0
 *** fork a thread (i.e. with a TID not a PID) with same address space
 *** mirror
 ** form 1
 *** fork a thread, sharing recursively bitmapped address space
 **** how far to recurse?
 **** could also use this for COW
 ***** pfork
 ***** tpfork
 *** tclone
 ** form 3
 *** clone, but with a TID
 *** tfork
 * tdestroy
 ** exit, but with a TID
 ** any TID in a PID can exit any TID
 * swap
 ** mark a page as not present
 ** on some (to my knowledge all MMU) platforms this can also give a note
 *** lets page fault handler find the process in swap space, e.g.
